# 逸仙狮机械组培训任务
* Solidworks的安装
    * ftp://172.18.196.202/
    * 账户：lion_public，密码：public
    * 安装教程[参考](https://mp.weixin.qq.com/s/INHVTvYIwF5ciqAETKCJmQ)
* 基本功能入门
    * 系统学习基本操作，学习推荐[链接](http://www.xmj1688.com/SolidWorksdzs/7920.html)中的前四章内容.
* 装配实战
    * 完成这两个开源[工程](https://share.weiyun.com/5ZNQZx1)的装配工作。
* 设计实战
    * 设计需求：进行机器人射击测试时，实验室地面经常散落着大量直径为17mm的小子弹，人工捡球低效又麻烦。请设计一个方便捡球的结构，可参考[网球捡球设计](https://item.taobao.com/item.htm?id=548041608959&ali_refid=a3_420434_1006:1106718742:N:%E7%BD%91%E7%90%83%E6%8D%A1%E7%90%83:11c79c409178e0de577e8619fcf882f5&ali_trackid=1_11c79c409178e0de577e8619fcf882f5&spm=a230r.1.1957635.6)。
    * 设计需求：设计一个结构，将弹仓里17mm的子弹一颗一颗从落弹口送出去。
* 请各位自行阅读规则RM2018机甲大师赛比赛规则手册V2.0.pdf, 也可观看加[比赛视频](https://www.skypixel.com/users/robomas-_user)加深对比赛的理解。